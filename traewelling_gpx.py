#!/usr/bin/python3

# SPDX-FileCopyrightText: 2024 Dominik George <nik@naturalnet.de>
#
# SPDX-License-Identifier: Apache-2.0

import logging
from datetime import datetime
from pathlib import Path
from typing import Optional

import gpx
import requests
import typer
from rich.logging import RichHandler

CREATOR = "traewelling_gpx"

app = typer.Typer()

logging.basicConfig(
    level="INFO", format="%(message)s", datefmt="[%X]", handlers=[RichHandler()]
)
log = logging.getLogger(__name__)


def status_geojson_to_gpx(user: dict, status: dict, geojson: dict) -> gpx.GPX:
    log.debug("Converting GeoJSON for status %d to GPX", status["id"])

    desc = (
        f"{user['displayName']}'s journey with {status['train']['lineName']} "
        f"from {status['train']['origin']['name']} "
        f"to {status['train']['destination']['name']}"
    )

    meta = gpx.metadata.Metadata()
    meta.desc = desc
    meta.time = datetime.fromisoformat(status["createdAt"])

    author = gpx.person.Person()
    author.name = user["displayName"]
    author_link = gpx.link.Link()
    author_link.href = f"https://traewelling.de/@{status['username']}"
    author_link.text = f"{user['displayName']} on Träwelling"
    author.link = author_link
    meta.author = author

    web_link = gpx.link.Link()
    web_link.href = f"https://traewelling.de/status/{status['id']}"
    web_link.text = f"{desc} on Träwelling"
    meta.links = [
        web_link,
    ]

    trkseg = gpx.track_segment.TrackSegment()
    trkpts = []
    for lon, lat in geojson["features"][0]["geometry"]["coordinates"]:
        point = gpx.waypoint.Waypoint()
        point.lat = gpx.types.Latitude(lat)
        point.lon = gpx.types.Longitude(lon)
        trkpts.append(point)
    trkseg.trkpts = trkpts

    track = gpx.track.Track()
    track.desc = desc
    track.cmt = status["body"] or None
    track.number = status["id"]
    if status["train"].get("operator"):
        track.src = f"{status['train']['operator']['name']} via Träwelling"
    else:
        track.src = "Träwelling"
    track.type = f"public_transport_{status['train']['category']}"
    track.trksegs = [trkseg]

    gpx_file = gpx.GPX()
    gpx_file.creator = CREATOR
    gpx_file.metadata = meta
    gpx_file.tracks = [track]

    return gpx_file


@app.command()
def main(
    token: str = typer.Option(..., help="Träwelling API token"),
    output_dir: Path = typer.Option(Path("."), help="Output directory for GPX files"),
):
    headers = {
        "Accept": "application/json",
        "Authorization": f"Bearer {token}",
    }

    log.debug("Configuring HTTP sesson for auto-retry and backoff")
    session = requests.Session()
    retries = requests.adapters.Retry(
        total=5, backoff_factor=10, status_forcelist=[429]
    )
    session.mount(
        "https://traewelling.de/api/",
        requests.adapters.HTTPAdapter(max_retries=retries),
    )

    log.debug("Loading user information")
    user = session.get(
        "https://traewelling.de/api/v1/auth/user", headers=headers
    ).json()["data"]
    log.info("Exporting statuses for user %s", user["username"])

    log.debug("Loading statuses")
    next_page = f"https://traewelling.de/api/v1/user/{user['username']}/statuses"
    while res := session.get(next_page, headers=headers).json():
        statuses = res["data"]
        for status in statuses:
            log.debug("Loading status id %d", status["id"])
            target_file = output_dir / f"status_{status['id']}.gpx"
            if target_file.exists():
                log.info("Skipping status %d (already exists)", status["id"])
                continue

            log.debug("Loading GeoJSON for status id %d", status["id"])
            geojson = session.get(
                f"https://traewelling.de/api/v1/polyline/{status['id']}"
            ).json()["data"]

            gpx_file = status_geojson_to_gpx(user, status, geojson)
            gpx_file.to_file(target_file)

            log.info("Exported status id %d", status["id"])

        if res["links"].get("next"):
            next_page = res["links"]["next"]
            log.debug("Loading next page")
        else:
            log.debug("No more pages")
            break

    log.info("Finished exporting statuses")


if __name__ == "__main__":
    app()
